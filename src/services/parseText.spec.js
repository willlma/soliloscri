import parseText from './parseText';

test('returns some parsed text', () => {
  const now = Date.now();
  const day = 864e5;
  const sentences = [{
    id: 1,
    text: 'I walked my dog',
    occurrences: [
      { timestamp: now, quantities: [1] },
      { timestamp: now - day, quantities: [1] },
      { timestamp: (now - day) * 3, quantities: [1] }
    ]
  }, {
    id: 2,
    text: 'I walked my cat',
    occurrences: [
      { timestamp: now - (day * 2), quantities: [1] },
      { timestamp: now - (day * 4), quantities: [1] }
    ]
  }, {
    id: 3,
    text: 'I walked Matt\'s cat',
    occurrences: [
      { timestamp: now - day, quantities: [1] },
      { timestamp: now - (day * 5), quantities: [1] }
    ]
  }, {
    id: 4,
    text: 'I walked Matt\'s dog',
    occurrences: [
      { timestamp: now - (day * 7), quantities: [1] },
      { timestamp: now - (day * 8), quantities: [1] }
    ]
  // }, {
  //   id: 5,
  //   text: 'I ran \\d miles',
  //   occurrences: [
  //     { timestamp: now, quantities: [3] },
  //     { timestamp: now - day, quantities: [4.5] },
  //     { timestamp: (now - day) * 3, quantities: [1.8] }
  //   ]
  }];
  const parsedText = [
    { commonSubstring: 'I walked my', ids: [1, 2], diffs: { dog: 3, cat: 2 } },
    {
      commonSubstring: 'I walked',
      ids: [1, 2, 3, 4],
      diffs: { "Matt's dog": 2, "Matt's cat": 2, 'my dog': 3, 'my cat': 2 },
    },
  ];

  expect(parseText(1, sentences)).toEqual(parsedText);
});
