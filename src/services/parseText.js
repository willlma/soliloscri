//@flow
import { Regex } from '../libs/constants';
import type { Sentence, Comparison } from '../libs/types';

function getComparison(commonSubstring, sentences): Comparison {
  const diffs = {};
  console.log('getComparison');
  const ids = sentences.map((sen) => {
    const diff = sen.text.slice(commonSubstring.length + 1);
    console.log(`diff: ${diff}`);
    diffs[diff] = sen.occurrences.reduce((acc, occurrence) => acc + occurrence.quantities[0], 0);
    return sen.id;
  });
  return { ids, commonSubstring, diffs };
}

export default function (id: number, sentences: Sentence[]) {
  const comparisons = [];
  const sentence = sentences.find((sen) => sen.id === id);
  if (!sentence) return comparisons;
  const { text } = sentence;
  const offsets = [];
  text.replace(Regex.wordEnds, (match, offset) => offsets.push(++offset));
  offsets.pop();
  offsets.reverse().reduce((previousSentencesLength, offset) => {
    // if (offset === text.length) return;
    const commonSubstring = text.slice(0, offset);
    const commonSentences = sentences.filter((sen) =>
      sen.text.slice(0, offset) === commonSubstring
    );
    const { length } = commonSentences;
    if (length <= 1 || length === previousSentencesLength) return 0;

    const comparison = getComparison(commonSubstring, commonSentences);
    comparisons.push(comparison);
    return commonSentences.length;
  }, 0);
  return comparisons;
}
