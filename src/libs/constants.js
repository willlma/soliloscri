export const day = 864e5;
export const days = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];
export const periods = ['Hours', 'Minutes', 'Seconds', 'Milliseconds'];
export const Regex = {
  wordEnds: /\w\b/g,
  number: /[\d.]+/
};
