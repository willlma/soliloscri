export type Sentence = {text: string, timestamp: number};
export type Comparison = { commonSubstring: string, diffs: { [string]: number }, ids?: number[] };
export type Action = { type: string };
