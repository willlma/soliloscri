export default {
  appendSentence: 'APPEND_SENTENCE',
  comparisons: 'COMPARISONS',
  mergeSentence: 'MERGE_SENTENCE',
  sentences: 'SENTENCES',
  selectedSentenceId: 'SELECTED_SENTENCE_ID',
};
