// @flow
import ActionTypes from 'soliloscri/src/libs/actionTypes';
import cloneDeep from 'lodash/cloneDeep';
import type { Sentence, Comparison, Action } from 'soliloscri/src/libs/types';
