// @flow
import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import { VictoryPie, VictoryTheme } from 'victory-native';
import type { Comparison as ComparisonT } from 'soliloscri/src/libs/types';

const normalizeData = (diffs) => Object.keys(diffs).map((key) => ({ x: key, y: diffs[key] }));

export default class Comparison extends PureComponent {
  props: ComparisonT
  state = { labelIndex: 0 }
  // onPress = () => {
  //   this.setState({ labelIndex: ++this.state.labelIndex })
  // }
  // events = [{
  //   target: 'data', //'labels']
  //   eventHandlers: { onPress: () => this.onPress() }
  // }]

  render() {
    const { commonSubstring, diffs } = this.props;
    // this.data = normalizeData(diffs);
    return (
      <View>
        <Text>{commonSubstring}</Text>
        <VictoryPie
          height={200} width={300} theme={VictoryTheme.material}
          data={normalizeData(diffs)} labels={this.label}
          events={[{
            target: 'data', //'labels']
            eventHandlers: {
              onPress: () => {
                const labelIndex = this.state.labelIndex + 1;
                this.setState({ labelIndex });
              }
            }
          }]}
        />
      </View>
    );
  }


  label = (datum: { x: number, y: number}) => {
    const { x, y } = datum;
    const diffs: { [string]: number } = this.props.diffs;
    const total = Object.values(diffs).reduce(
      (accumulator, currentValue: number) => accumulator + currentValue, 0
    );
    const proportion = y / total;
    return [x, y, `${(proportion) * 100}%`][this.state.labelIndex];
  }
}
