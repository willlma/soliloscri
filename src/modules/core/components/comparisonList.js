// @flow
import React from 'react';
import { View } from 'react-native';
import type { Comparison as ComparisonT } from 'soliloscri/src/libs/types';
import Comparison from './comparison';

type Props = { comparisons: ComparisonT[] }
export default ({ comparisons }: Props) => (comparisons ?
  <View>{comparisons.map(({ commonSubstring, diffs }) =>
    <Comparison key={commonSubstring} commonSubstring={commonSubstring} diffs={diffs} />
  )}</View> :
  null
);

// const styles = StyleSheet.create({});
