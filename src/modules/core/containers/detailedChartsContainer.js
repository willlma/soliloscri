// @flow
import React from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { doubleMarginWidth } from 'soliloscri/src/styles/variables';
import type { Sentence, Comparison } from 'soliloscri/src/libs/types';
import WeekChart from '../components/weekChart';
import Comparisons from '../components/comparisonList';

const getSentence = (id, sentences) => sentences.find((sen) => sen.id === id);

type Props = {
  id: number,
  comparisons: Comparison[],
  sentences: Sentence[],
}
const DetailedChartsContainer = ({ id, comparisons, sentences }: Props) => (
  <ScrollView style={{ padding: doubleMarginWidth }}>
    <WeekChart sentence={getSentence(id, sentences)} />
    <Comparisons comparisons={comparisons} />
  </ScrollView>
);

DetailedChartsContainer.navigationOptions = ({ navigation }) => ({
  title: navigation.state.params.title
});

const mapStateToProps = (state) => ({
  id: state.selectedSentenceId,
  comparisons: state.comparisons,
  sentences: state.sentences,
});

export default connect(mapStateToProps/*, dispatchProps*/)(DetailedChartsContainer);
